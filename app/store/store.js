import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import data from './modules/data/store.js';

export default new Vuex.Store({
    strict: true,
    modules: {
        data
    }
});
