import items from '../../../items.json';
import origins from '../../../origins.json';
import classes from '../../../classes.json';
import champions from '../../../champions.json';

const state = {
    items: items,
    origins: origins,
    classes: classes,
    champions: champions
}

export default state
