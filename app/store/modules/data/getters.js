const getters = {
    baseItems (state) {
        return state.items.filter(item => {
            return item.baseItem
        });
    },
    sortedChampions: (state) => (sortBy) => {
        let champions = state.champions.slice(0);

        champions.sort((first, second) => {
            if (sortBy === 'name') {
                return first.name.localeCompare(second.name)
            } else if (sortBy === 'cost') {
                return first.cost - second.cost
            }
        })

        return champions
    },
    championsByClass: (state) => (value) => {
        let champions = state.champions.map(champion => {
            champion.name.toLowerCase();
            return champion;
        })
        return champions.filter(champion => {
            return champion.class.indexOf(value) !== -1;
        })
    },
    championsByOrigin: (state) => (origin) => {
        let champions = state.champions.map(champion => {
            champion.name.toLowerCase();
            return champion;
        })
        return champions.filter(champion => {
            return champion.origin.indexOf(origin) !== -1;
        })
    }
}

export default getters
