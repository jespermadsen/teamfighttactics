export default {
    methods: {
        championBorderColor(cost) {
            switch (cost) {
                case 1:
                    return '#485767';

                case 2:
                    return '#099853';

                case 3:
                    return '#3FA4CD';

                case 4:
                    return '#E50DC2';

                case 5:
                    return '#DAAA29';
            }
        }
    }
}