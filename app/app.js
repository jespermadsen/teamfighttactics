import Vue from 'nativescript-vue';
import Vuex from 'vuex';
import VueDevTools from 'nativescript-vue-devtools';

// Vue.use(VueDevTools, {
//     host: '192.168.1.103'
// });

global.firebase = require('nativescript-plugin-firebase');

import { messaging } from "nativescript-plugin-firebase/messaging";

firebase.init({
    showNotificationsWhenInForeground: true,
    onMessageReceivedCallback: function(message) {
        console.log("Title: " + message.title);
        console.log("Body: " + message.body);
        // if your server passed a custom property called 'foo', then do this:
        console.log("Value of 'foo': " + message.data.foo);
    }
});

import champion from './mixins/champion';

Vue.mixin(champion);

import store from './store/store';

import {TNSFontIcon, fonticon} from 'nativescript-fonticon';

// TNSFontIcon.debug = true;
TNSFontIcon.paths = {
    'fa': './assets/css/fontawesome.min.css',
    'fal': './assets/css/light.min.css',
    'far': './assets/css/regular.min.css',
    'fas': './assets/css/solid.min.css',
    'fab': './assets/css/brands.min.css'
};
TNSFontIcon.loadCss();

Vue.filter('fonticon', fonticon);

import Frontpage from './assets/pages/Frontpage';

Vue.registerElement('SVGImage', () => require('@teammaestro/nativescript-svg').SVGImage)
Vue.registerElement('RadSideDrawer', () => require('nativescript-ui-sidedrawer').RadSideDrawer)

// Uncommment the following to see NativeScript-Vue output logs
Vue.config.silent = true

Vue.use(Vuex)

import Main from './assets/Main'
import ChampionImage from './assets/components/ChampionImage'
import FontIcon from './assets/components/FontIcon'

Vue.component('Main', Main)
Vue.component('ChampionImage', ChampionImage)
Vue.component('FontIcon', FontIcon)

import router from './router';
Vue.prototype.$router = router;

Vue.prototype.$goto = function (to, props) {
    var options = options || {
        frame: 'mainFrame',
        clearHistory: false,
        backstackVisible: true,
        transition: {
            name: "fade",
            duration: 100,
            curve: "linear"
        },
        props: props
    }

    firebase.analytics.logEvent({
        key: 'page_view',
        parameters: [
            {
                key: 'page_id',
                value: to
            }
        ]
    }).catch((error) => {
        console.error(error)
    })

    this.$navigateTo(this.$router[to], options)
}

new Vue({
    template: `
        <Frame id="mainFrame" backgroundColor="#0b2b3d">
            <Frontpage />
        </Frame>
    `,
    store: store,
    components: {
        Frontpage
    },
}).$start()
