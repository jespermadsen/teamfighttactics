import Frontpage from "./assets/pages/Frontpage";
import Champion from './assets/pages/Champion';
import ChampionDropRates from './assets/pages/ChampionDropRates';
import Champions from './assets/pages/Champions';
import Class from './assets/pages/Class';
import Classes from './assets/pages/Classes';
import Item from './assets/pages/Item';
import Items from './assets/pages/Items';
import Origin from './assets/pages/Origin';
import Origins from './assets/pages/Origins';
import RankedExplained from './assets/pages/RankedExplained';

const router = {
    Frontpage,
    Champion,
    ChampionDropRates,
    Champions,
    Class,
    Classes,
    Item,
    Items,
    Origin,
    Origins,
    RankedExplained
}

export default router
